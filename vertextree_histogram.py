import os,sys
import ROOT
from math import *
from ROOT import *
import numpy as np
import LHCbStyle


f = TFile("outfile_27_07_2017_v4.root")
v = f.Get("verttree")

histtimecanv = TCanvas( 'histtimecanv', 'histtimecanv',200, 10, 700, 500 )


z_pos = TH1D("z_pos","z_pos", 100,-300,300)
ntracks = TH1D("ntracks","ntracks", 55,0,55)


##verttree.Draw("pv_time>>pv_time")
verttree.Draw("pv_res>>pv_res")
#verttree.Draw("ntracks>>ntracks")
for entry in v:
	
	z_pos.Fill(v.zpos)
	ntracks.Fill(v.ntracks)
	xz_plot.Fill(h.zpos_hit,h.xpos_hit)
	yz_plot.Fill(h.zpos_hit,h.xpos_hit)

z_pos.GetXaxis().SetTitle("z(mm)")
z_pos.GetYaxis().SetTitle("N(PVs)")
z_pos.Draw()
histtimecanv.SaveAs("z_pos.png")

pv_time.GetXaxis().SetTitle("pv time(ps)")
pv_time.GetYaxis().SetTitle("N(PVs)")
pv_time.Draw()
histtimecanv.SaveAs("pv_time.png")

pv_res.GetXaxis().SetTitle("pv res(ps)")
pv_res.GetYaxis().SetTitle("N(PVs)")
pv_res.Draw()
histtimecanv.SaveAs("pv_res.png")

ntracks.GetXaxis().SetTitle("number of tracks")
ntracks.GetYaxis().SetTitle("N(PVs)")
ntracks.Draw()
histtimecanv.SaveAs("ntracks.png")


