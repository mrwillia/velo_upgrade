
import os,sys
import ROOT as R
from math import *
from ROOT import *
import numpy as np
from array import array


R.gROOT.SetBatch(True)
R.gROOT.ProcessLine(".x lhcbstyle.C")

debug = False

inntimeres = 200 #ps
outtimeres = 200 #ps
d1 = 10 #mm the dimension of the square where the beam passes
d2 = 15 #mm the innerspace
d3 = 15 #mm the outerspace
modulespacesep = [-277.,-252.,-227.,-202.,-132.,-62.,-37.,-12.,13.,38.,63.,88.,113.,138.,163.,188.,213.,238.,263.,325.,402.,497.,616.,661.,706.,751.]
ngen = 3000 #number of events

mfile  = TFile.Open("Brunel-VELOPP_nu76_VELOonly_10evt.root")
mfile1 = TFile.Open("Ganga1927_TrackEta_AllGeneratedStableChargedParticles.root")
pvmult_histo = mfile.Get("TrackIPResolutionChecker/PV/RecoPVNumTracksH1");
eta_histo    = mfile1.Get("44")
lumi_histo   = mfile.Get("Track/PrChecker/Velo/nPV_Events")


bresolution = [0.02,0.02,0.3] #mm
pvresolution = [0.02,0.02,0.105] #mm 

bmomres           = [0.005,0.005,0.005] # %
bmomcentre        = [1.0,1.0,1.0] # %

beamspread        = [0.0377,0.0377,63.] # mm
beamspread_time   = 250 #ps

sigmathreshold    = 3. 

bmomfile = TFile.Open("B_kinematics.root")
bmom_x_histo = bmomfile.Get("px")
bmom_y_histo = bmomfile.Get("py")
bmom_z_histo = bmomfile.Get("pz")

gRandom.SetSeed(0)
rnd = TRandom3()
rnd.SetSeed(0)

min_hits = 3
min_tracks = 2
mean_nPVs = 55

outfile_name = "outfile_" + repr(inntimeres) + "_" + repr(outtimeres) + ".root"
outfile = TFile(outfile_name, "recreate")

hist_sep1D = TH1F("hist_sep1D","hist_sep1D",2000,-1.0,20)
hist_sep2D = TH2F("hist_sep2D","hist_sep2D",2000,-1.0,20,500,-1500,1500)
blifetimeresidual = TH1F("blifetimeresidual","blifetimeresidual",200,-3,3)
blifetimeresidualwithmismatch = TH1F("blifetimeresidualwithmismatch","blifetimeresidualwithmismatch",200,-3,3)

trackTree = TTree("tracktree", "my tree")
eta = np.zeros(1, dtype=float)
phi = np.zeros(1, dtype=float)
nhits = np.zeros(1, dtype=float)
nhits_in = np.zeros(1, dtype=float)
nhits_out = np.zeros(1, dtype=float)
track_res = np.zeros(1, dtype=float)
track_time_diff = np.zeros(1, dtype=float)
trackTree.Branch('eta', eta, 'eta/D')
trackTree.Branch('phi', phi, 'phi/D')
trackTree.Branch('nhits', nhits, 'nhits/D')
trackTree.Branch('nhits_in', nhits_in, 'nhits_in/D')
trackTree.Branch('nhits_out', nhits_out, 'nhits_out/D')
trackTree.Branch('track_res', track_res, 'track_res/D')
trackTree.Branch('track_time_diff', track_time_diff, 'track_time_diff/D')

hitTree = TTree("hittree", "my tree")
hit_res = np.zeros(1, dtype=float)
xpos_hit = np.zeros(1, dtype=float)
ypos_hit = np.zeros(1, dtype=float)
zpos_hit = np.zeros(1, dtype=float)
module_num = np.zeros(1, dtype=float)
hitTree.Branch('hit_res', hit_res, 'hit_res/D')
hitTree.Branch('xpos_hit', xpos_hit, 'xpos_hit/D')
hitTree.Branch('ypos_hit', ypos_hit, 'ypos_hit/D')
hitTree.Branch('zpos_hit', zpos_hit, 'zpos_hit/D')
hitTree.Branch('module_num', module_num, 'module_num/D')

vertexTree = TTree("verttree", "my tree")
vert = np.zeros(1, dtype=float)
xpos = np.zeros(1, dtype=float)
ypos = np.zeros(1, dtype=float)
zpos = np.zeros(1, dtype=float)
ntracks = np.zeros(1, dtype=int)
pv_time = np.zeros(1, dtype=float)
pv_res = np.zeros(1, dtype=float)
nhits_vertex_in  = np.zeros(1, dtype=int)
nhits_vertex_out = np.zeros(1, dtype=int)
nhits_vertex     = np.zeros(1, dtype=int)
t_diff = np.zeros(1, dtype=float)
t_diff_sig = np.zeros(1, dtype=float)
t_diff_sig_v2 = np.zeros(1, dtype=float)
ip = np.zeros(1, dtype=float)
vertexTree.Branch('vert', vert, 'vert/D')
vertexTree.Branch('xpos', xpos, 'xpos/D')
vertexTree.Branch('ypos', ypos, 'ypos/D')
vertexTree.Branch('zpos', zpos, 'zpos/D')
vertexTree.Branch('ntracks', ntracks, 'ntracks/I')
vertexTree.Branch('pv_time', pv_time, 'pv_time/D')
vertexTree.Branch('pv_res', pv_res, 'pv_res/D')
vertexTree.Branch('nhits_vertex_in',  nhits_vertex_in,  'nhits_vertex_in /I')
vertexTree.Branch('nhits_vertex_out', nhits_vertex_out, 'nhits_vertex_out/I')
vertexTree.Branch('nhits_vertex',     nhits_vertex,     'nhits_vertex    /I')
vertexTree.Branch('t_diff', t_diff, 't_diff/D')
vertexTree.Branch('t_diff_sig', t_diff_sig, 't_diff_sig/D')
vertexTree.Branch('t_diff_sig_v2', t_diff_sig_v2, 't_diff_sig_v2/D')
vertexTree.Branch('ip', ip, 'ip/D')

eventTree = TTree("eventtree", "my tree")
num_pvs = np.zeros(1, dtype=float)
b_momentum_mag = np.zeros(1, dtype=float)
bx_momentum = np.zeros(1, dtype=float)
by_momentum = np.zeros(1, dtype=float)
bz_momentum = np.zeros(1, dtype=float)
sv_pos_mag = np.zeros(1, dtype=float)
sv_xpos = np.zeros(1, dtype=float)
sv_ypos = np.zeros(1, dtype=float)
sv_zpos = np.zeros(1, dtype=float)
sv_time = np.zeros(1, dtype=float)
sv_res = np.zeros(1, dtype=float)
svpos_smeared_mag = np.zeros(1, dtype=float)
sv_xpos_smeared = np.zeros(1, dtype=float)
sv_ypos_smeared = np.zeros(1, dtype=float)
sv_zpos_smeared = np.zeros(1, dtype=float)
bmom_smeared_mag = np.zeros(1, dtype=float)
bx_mom_smeared = np.zeros(1, dtype=float)
by_mom_smeared = np.zeros(1, dtype=float)
bz_mom_smeared = np.zeros(1, dtype=float) 
blifetime = np.zeros(1, dtype=float)
dec_length = np.zeros(1, dtype=float)
distPV_min = np.zeros(1, dtype=float)
numbfliespast = np.zeros(1, dtype=float)
iptobpv = np.zeros(1, dtype=float)
blifetime_IP = np.zeros(1, dtype=float)
blifetime_2D = np.zeros(1, dtype=float)
blifetime_2D_v2 = np.zeros(1, dtype=float)
correctPV_IP = np.zeros(1, dtype=float)
correctPV_2D = np.zeros(1, dtype=float)
correctPV_2D_v2 = np.zeros(1, dtype=float)
eventTree.Branch('num_pvs', num_pvs, 'num_pvs/D')
eventTree.Branch('b_momentum_mag', b_momentum_mag, 'b_momentum_mag/D')
eventTree.Branch('bx_momentum', bx_momentum, 'bx_momentum/D')
eventTree.Branch('by_momentum', by_momentum, 'by_momentum/D')
eventTree.Branch('bz_momentum', bz_momentum, 'bz_momentum/D')
eventTree.Branch('dec_length', dec_length, 'dec_length/D')
eventTree.Branch('sv_pos_mag', sv_pos_mag, 'sv_pos_mag/D')
eventTree.Branch('sv_xpos', sv_xpos, 'sv_xpos/D')
eventTree.Branch('sv_ypos', sv_ypos, 'sv_ypos/D')
eventTree.Branch('sv_zpos', sv_zpos, 'sv_zpos/D')
eventTree.Branch('sv_time', sv_time, 'sv_time/D')
eventTree.Branch('sv_res', sv_res, 'sv_res/D')
eventTree.Branch('svpos_smeared_mag', svpos_smeared_mag, 'svpos_smeared_mag/D')
eventTree.Branch('sv_xpos_smeared', sv_xpos_smeared, 'sv_xpos_smeared/D')
eventTree.Branch('sv_ypos_smeared', sv_ypos_smeared, 'sv_ypos_smeared/D')
eventTree.Branch('sv_zpos_smeared', sv_zpos_smeared, 'sv_zpos_smeared/D')
eventTree.Branch('bmom_smeared_mag', bmom_smeared_mag, 'bmom_smeared_mag/D')
eventTree.Branch('bx_mom_smeared', bx_mom_smeared, 'bx_mom_smeared/D')
eventTree.Branch('by_mom_smeared', by_mom_smeared, 'by_mom_smeared/D')
eventTree.Branch('bz_mom_smeared', bz_mom_smeared, 'bz_mom_smeared/D')
eventTree.Branch('blifetime', blifetime, 'blifetime/D')
eventTree.Branch('iptobpv', iptobpv, 'iptobpv/D')
eventTree.Branch('numbfliespast', numbfliespast, 'numbfliespast/D')
eventTree.Branch('distPV_min', distPV_min, 'distPV_min/D')
eventTree.Branch('blifetime_IP', blifetime_IP, 'blifetime_IP/D')
eventTree.Branch('blifetime_2D', blifetime_2D, 'blifetime_2D/D')
eventTree.Branch('blifetime_2D_v2', blifetime_2D_v2, 'blifetime_2D_v2/D')
eventTree.Branch('correctPV_IP', correctPV_IP, 'correctPV_IP/D')
eventTree.Branch('correctPV_2D', correctPV_2D, 'correctPV_2D/D')
eventTree.Branch('correctPV_2D_v2', correctPV_2D_v2, 'correctPV_2D_v2/D')


#Lifetime calculation between secondary and primary vertex
def complifetime(bdecvertex,pv,bmom) :
  distance = sqrt(pow(bdecvertex[2]-pv[2],2)+\
                  pow(bdecvertex[1]-pv[1],2)+\
                  pow(bdecvertex[0]-pv[0],2) )                  
  return 5279.*distance/(0.3*bmom.Mag())
  
  
#get the x and y position of the particle passing through modules  
def   getpxy(eta,phi,module,pvzpos):
     pxy = []
     theta = 2*atan(exp(-eta))
     x = (module - pvzpos)*tan(theta)*cos(phi)
     y = (module - pvzpos)*tan(theta)*sin(phi)
     pxy.append(x)
     pxy.append(y)
     return pxy
     

# ================================================================================= calcverttime() function
# =================================================================================

def calcverttime(pvmult,pvtimepos,pvzpos) :
	    
  ngoodtracks = 0
  nhits_vert_inn_arr = []
  nhits_vert_out_arr = []
  nhits_vert_arr = []

  t_track_arr = []
  track_res_arr = []
  
  ## ================================================= Track loop
  while ngoodtracks < pvmult :
	    
    #for tracks in range(0,pvmult) :
		        
    eta[0] = eta_histo.GetRandom()
    phi[0] = gRandom.Uniform(-pi,pi)
		        
    temp_res = -2
    temp_resin = -2
    temp_resout = -2
    timeres_hits = []
    timeres_hitsin = []
    timeres_hitsout = []
    time_hits = []
    timeres_hits_len = []

    if event is 0 and debug : print "Generating track with eta: " + repr(eta[0]) + ", phi: " + repr(phi[0]) + ", PVz: " + repr(pvzpos) 

    ## ================================================= Module loop
    for i,module in enumerate(modulespacesep):
      xtemp = getpxy(eta[0],phi[0],module,pvzpos)[0]
      ytemp = getpxy(eta[0],phi[0],module,pvzpos)[1]
      message = " "

      if eta[0] > 0 and module < pvzpos : continue #for forward tracks there are only hits in modules at larger-z than PV
      if eta[0] < 0 and module > pvzpos : continue # "  backward "       "    "    "    "   "    "     " smaller-z "   "

				
      if abs(xtemp) < (d1/2) and abs(ytemp) < (d1/2):		                        
        temp_res = -1
        temp_resin = -1
        temp_resout = -1
      elif abs(xtemp) <= (d2+d1/2) and abs(ytemp) <= (d2+d1/2):
        temp_res = inntimeres
        temp_resin = inntimeres
        temp_resout = -1	     
      elif  abs(xtemp) <= (d3+d2+d1/2) and abs(ytemp) <= (d3+d2+d1/2):
        temp_res = outtimeres
        temp_resout = outtimeres
        temp_resin = -1
      else:
        temp_res = -1
        temp_resin = -1
        temp_resout = -1
		                   
      if  temp_res > 0 :     	
        timeres_hits.append(temp_res)
        time_hits.append(gRandom.Gaus(pvtimepos,temp_res))
        hit_res[0] = temp_res
        xpos_hit[0] = xtemp
        ypos_hit[0] = ytemp
        zpos_hit[0] = module
        module_num[0] = i
        if hittree.GetEntries() < 100000 : hitTree.Fill() 

      if  temp_resin > 0 :
        timeres_hitsin.append(temp_resin)
        message = " **IN** "

      if  temp_resout > 0 :
        timeres_hitsout.append(temp_resout)
        message = " **OUT** "

      if ngoodtracks is 0 and event is 0 and debug : print "ModuleZ: " + repr(module) + ", x: " + repr(xtemp) + ", y: " + repr(ytemp) + " " + message

      
    ## ================================================= End of module loop
        
    if event is 0 and debug : print "Finished loop over modules. This track has " + repr(len(timeres_hits)) + " hits in total"

    if len(timeres_hits) < min_hits : continue #After this, all tracks are 'good'
    ngoodtracks += 1

    nhits[0]     = len(timeres_hits)
    nhits_in[0]  = len(timeres_hitsin)
    nhits_out[0] = len(timeres_hitsout)

    # weighted average of hit times
    sum_wt = 0.
    sum_timeresw = 0.
    for hit in range(0,len(timeres_hits)):
      weight_hits = 1.0/timeres_hits[hit]/timeres_hits[hit] 
      wt = weight_hits*time_hits[hit]
      sum_timeresw += weight_hits 
      sum_wt += wt 
    if sum_timeresw > 0:
      t_track = sum_wt/sum_timeresw
      track_res[0] = sqrt(1/sum_timeresw)
      track_time_diff[0] = t_track - pvtimepos
      t_track_arr.append(sum_wt/sum_timeresw)
      track_res_arr.append(sqrt(1/sum_timeresw))

    nhits_vert_arr.append(nhits[0])
    nhits_vert_inn_arr.append(nhits_in[0])
    nhits_vert_out_arr.append(nhits_out[0])
    if tracktree.GetEntries() < 100000 : trackTree.Fill()
      
  ## ================================================= End of track loop

                
  # weighted average of track times
  sum_wtvert = 0.
  sum_timeresw_vert = 0.
  for track in range(0,len(track_res_arr)):	
    weight_tracks = 1.0/track_res_arr[track]/track_res_arr[track] 
    wt_vert = weight_tracks*t_track_arr[track]
    sum_timeresw_vert += weight_tracks 
    sum_wtvert += wt_vert 

  if sum_timeresw_vert > 0:
    t_vert = sum_wtvert/sum_timeresw_vert
    vert_res = sqrt(1/sum_timeresw_vert)


  nhits_vert     = sum(nhits_vert_arr)
  nhits_vert_in  = sum(nhits_vert_inn_arr)	
  nhits_vert_out = sum(nhits_vert_out_arr)

  if event is 0 and debug :
    print "Finished loop over tracks. This PV has " + repr(len(track_res_arr)) + " tracks, and " + repr(nhits_vert) + " hits.\n\n"
    for track in range(0,len(track_res_arr)): print nhits_vert_arr[track]

					    
  return t_vert,vert_res,nhits_vert_in,nhits_vert_out,nhits_vert		

# ================================================================================= End of calcverttime() function
# =================================================================================


                
madeplot = False  #for example event plot, just need to pick one (suitable) event

for event in range(0,ngen) :
  evtTimeFlag = True
  pvs = { "cart_pos" : [], 
          "time_pos" : [],
          "trk_mult" : [] }
  lumi = gRandom.Poisson(mean_nPVs)
  num_pvs[0] = lumi 

  numbfliespast_temp = 0

  sigma_ip = 0.0315
  sepIP_min = 1000
  sep2D_min = 1000
  sep2D_min_v2 = 1000
  bestvert_IP = -1
  bestvert_2D = -1
  bestvert_2D_v2 = -1

  distPV_min_temp = 1000
	       
  ## ================================================= Vertex loop
  for vertex in range(0,lumi) :
    vert[0] = vertex
    # generate pv 
    pvs["cart_pos"].append([gRandom.Gaus(0,beamspread[0]),gRandom.Gaus(0,beamspread[1]),gRandom.Gaus(0,beamspread[2])])
    pvs["time_pos"].append(gRandom.Gaus(0,beamspread_time))
    pvs["trk_mult"].append(int(pvmult_histo.GetRandom()))
	    
    xpos[0] = pvs["cart_pos"][vertex][0]
    ypos[0] = pvs["cart_pos"][vertex][1]
    zpos[0] = pvs["cart_pos"][vertex][2]
    ntracks[0] = pvs["trk_mult"][vertex]

    # get the PV time resolution
    pv_time_info = calcverttime(pvs["trk_mult"][vertex],pvs["time_pos"][vertex],pvs["cart_pos"][vertex][2])
    pv_time[0]   = pv_time_info[0]
    pv_res[0]    = pv_time_info[1]

    # number of hits per vertex
    nhits_vertex_in[0]  = pv_time_info[2]		    
    nhits_vertex_out[0] = pv_time_info[3]		   
    nhits_vertex[0]     = pv_time_info[4]
    
			
    # get the PV position smeared 
    smearedpv = [ pvs["cart_pos"][vertex][0]+gRandom.Gaus(0,pvresolution[0]),
                  pvs["cart_pos"][vertex][1]+gRandom.Gaus(0,pvresolution[1]),
                  pvs["cart_pos"][vertex][2]+gRandom.Gaus(0,pvresolution[2])]


    ## ================================================= b PV
    if vertex is 0:

      # generate a b
      bmom = TVector3(bmom_x_histo.GetRandom(),bmom_y_histo.GetRandom(),bmom_z_histo.GetRandom())
      bmomunit = bmom.Unit()
      btau  = gRandom.Exp(1.5)
      bctau = 0.3*(bmom.Mag()/5279.)*btau
      b_momentum_mag[0] = bmom.Mag()
      bx_momentum[0] = bmom(0)
      by_momentum[0] = bmom(1)
      bz_momentum[0] = bmom(2)
      dec_length[0] = bctau
      ntracks_fromb = 2
        
      sv_xpos[0] = pvs["cart_pos"][vertex][0]+bmomunit[0]*bctau
      sv_ypos[0] = pvs["cart_pos"][vertex][1]+bmomunit[1]*bctau
      sv_zpos[0] = pvs["cart_pos"][vertex][2]+bmomunit[2]*bctau
      sv_pos_mag[0] = sqrt(sv_xpos[0]**2 + sv_ypos[0]**2 + sv_zpos[0]**2)
        
      # get b decay vertex,smeared
      bdecvertex = [sv_xpos[0]+gRandom.Gaus(0,bresolution[0]),\
                    sv_ypos[0]+gRandom.Gaus(0,bresolution[1]),\
                    sv_zpos[0]+gRandom.Gaus(0,bresolution[2])]
      sv_xpos_smeared[0] = bdecvertex[0]
      sv_ypos_smeared[0] = bdecvertex[1]
      sv_zpos_smeared[0] = bdecvertex[2]
      svpos_smeared_mag[0] = sqrt(bdecvertex[0]**2 + bdecvertex[1]**2 + bdecvertex[2]**2)

      # get the SV time resolution			
      sv_time_info = calcverttime(ntracks_fromb,pvs["time_pos"][vertex],pvs["cart_pos"][vertex][2])
      sv_time[0] = sv_time_info[0]
      sv_res[0] = sv_time_info[1]
			 
		
      # get the smeared momentum
      bmomsmeared = bmom
      bmomsmeared.SetX(bmomsmeared.Px()*gRandom.Gaus(bmomcentre[0],bmomres[0]))
      bmomsmeared.SetY(bmomsmeared.Py()*gRandom.Gaus(bmomcentre[1],bmomres[1]))
      bmomsmeared.SetZ(bmomsmeared.Pz()*gRandom.Gaus(bmomcentre[2],bmomres[2]))
      bmomsmearedunit = bmomsmeared.Unit()

      bmom_smeared_mag[0] = bmomsmeared.Mag()
      bx_mom_smeared[0] = bmomsmeared(0)
      by_mom_smeared[0] = bmomsmeared(1)
      bz_mom_smeared[0] = bmomsmeared(2)

    ## ================================================= End of b PV


    # get the measured B tau
    btaumeas = complifetime(bdecvertex,smearedpv,bmomsmeared)
    if vertex is 0 : 
      blifetime[0] = btaumeas
      blifetimeresidual.Fill(blifetime[0] - btau) #for vertex = 0
		    
    # get the time difference between b and pv 
    t_diff[0] = sv_time[0] - pv_time[0]
    t_diff_sig[0] = t_diff[0]/(sqrt(sv_res[0]**2 + pv_res[0]**2))
    t_diff_sigma = sqrt(sv_res[0]**2 + pv_res[0]**2)
    t_diff_sig_v2[0] = (t_diff[0] + btau - btaumeas)/t_diff_sigma

    # distance to the closest pv from the bpv
    dist = sqrt((xpos[0] - sv_xpos[0])**2 + (ypos[0] - sv_ypos[0])**2 + (zpos[0] - sv_zpos[0])**2)
    if dist < distPV_min_temp : distPV_min_temp = dist			
		
    # Calculate IP 
    decdotmom = ((bdecvertex[0]-smearedpv[0])*bmomsmearedunit[0]+\
                 (bdecvertex[1]-smearedpv[1])*bmomsmearedunit[1]+\
                 (bdecvertex[2]-smearedpv[2])*bmomsmearedunit[2])
    IP = TVector3(bdecvertex[0] - smearedpv[0] - decdotmom*bmomsmearedunit[0],\
                  bdecvertex[1] - smearedpv[1] - decdotmom*bmomsmearedunit[1],\
                  bdecvertex[2] - smearedpv[2] - decdotmom*bmomsmearedunit[2])
    ip[0] = IP.Mag()	   
    IP_sig = ip[0]/sigma_ip

    if vertex is 0:        #special case for true b PV
      iptobpv[0] = ip[0]
      bpvmeas = smearedpv[2]
      pv_time_vert0 = pv_time[0]
        

    if smearedpv[2] < bdecvertex[2] and smearedpv[2] > bpvmeas:
      numbfliespast_temp += 1
		    
    if ip[0] < sepIP_min :
      sepIP_min = ip[0]
      bestvert_IP = vertex
      btaumeas_IP = complifetime(bdecvertex,smearedpv,bmomsmeared)
      blifetime_IP[0] = btaumeas_IP

    if sqrt(IP_sig**2 + t_diff_sig[0]**2) < sep2D_min :
      sep2D_min = sqrt(IP_sig**2 + t_diff_sig[0]**2)
      bestvert_2D = vertex
      btaumeas_2D = complifetime(bdecvertex,smearedpv,bmomsmeared)
      blifetime_2D[0] = btaumeas_2D

    if sqrt(IP_sig**2 + t_diff_sig_v2[0]**2) < sep2D_min_v2 :
      sep2D_min_v2 = sqrt(IP_sig**2 + t_diff_sig_v2[0]**2)
      bestvert_2D_v2 = vertex
      btaumeas_2D_v2 = complifetime(bdecvertex,smearedpv,bmomsmeared)
      blifetime_2D_v2[0] = btaumeas_2D_v2
					
    if madeplot is False :     #keep making plot until it passes requirements (below)
      for i in range(0,10000) :
        fill1 = gRandom.Gaus(ip[0],sigma_ip)	    
        fill2 = gRandom.Gaus(t_diff[0],t_diff_sigma)	
        hist_sep1D.Fill(fill1)			 
        hist_sep2D.Fill(fill1,fill2)
				
    vertexTree.Fill()
  ## ================================================= End of vertex loop


  numbfliespast[0] = numbfliespast_temp
  distPV_min[0]    = distPV_min_temp


  ## == Did we pick the correct PV?
  if bestvert_IP is 0 : 
    correctPV_IP[0] = 1
  else:
    correctPV_IP[0] = 0
    blifetimeresidualwithmismatch.Fill(blifetime_IP[0] - btau)

  if bestvert_2D is 0 : 
    correctPV_2D[0] = 1
  else:
    correctPV_2D[0] = 0

  if bestvert_2D_v2 is 0 : 
    correctPV_2D_v2[0] = 1
  else:
    correctPV_2D_v2[0] = 0
	
	
  ## == Find event where timing resolves PV-misassociation, and save plots. numbfliespast requirement gives nicer plots (more separation in IP).
  ##    either (reset the plots), or (keep plots and set madeplot = True to stop further plot-making)
  if correctPV_IP[0] == 0 and correctPV_2D[0] == 1 and numbfliespast[0] < 4 and madeplot == False:
    print "Making separation plots for event " + repr(event)
    madeplot = True
    #hist_sep1D.Write()
    #hist_sep2D.Write()
    
    histcanv1 = TCanvas("histcanv1","histcanv1",600,400)
    hist_sep1D.GetXaxis().SetTitle("IP (mm)")
    hist_sep1D.Draw()
    histcanv1.SaveAs("hist_sep1D.pdf")
    hist_sep2D.GetXaxis().SetTitle("IP (mm)")
    hist_sep2D.GetYaxis().SetTitle("#delta t (ps)")
    hist_sep2D.Draw("COLZ")
    histcanv1.SaveAs("hist_sep2D.pdf")
        
  else:
    hist_sep1D.Reset()
    hist_sep2D.Reset()
	
  
  eventTree.Fill()
  if event % 100 == 0 : print "Processed " + repr(event) + " events"
  
  ## ================================================= End of event loop

# Fill lifetime residual plot outside event loop
#blifetimeresidual.Write()
#blifetimeresidualwithmismatch.Write()


gStyle.SetOptStat(0)
lifetimecanv = TCanvas("lifetimecanv","lifetimecanv")
blifetimeresidual.GetXaxis().SetTitle("b lifetime residual (ps)")
blifetimeresidual.GetXaxis().SetLabelSize(0.045)
blifetimeresidual.GetXaxis().SetTitleSize(0.045)
blifetimeresidual.GetYaxis().SetLabelSize(0.045)
blifetimeresidual.SetTitle("")
lifetimecanv.SetLogy()
blifetimeresidual.SetMarkerColor(2)
blifetimeresidual.SetMarkerStyle(4)
blifetimeresidual.SetLineColor(2)
blifetimeresidual.SetLineWidth(2)
blifetimeresidualwithmismatch.SetMarkerColor(4)
blifetimeresidualwithmismatch.SetMarkerStyle(4)
blifetimeresidualwithmismatch.SetLineColor(4)
blifetimeresidualwithmismatch.SetLineWidth(2)


blifetimeresidual.DrawNormalized("EP")
blifetimeresidualwithmismatch.DrawNormalized("EPSAME")

leg = TLegend(0.65, 0.75, 0.89, 0.89)
leg.AddEntry(blifetimeresidual, "Correct PV", "lp")
leg.AddEntry(blifetimeresidualwithmismatch, "Incorrect PV", "lp")
leg.SetTextSize(0.045)
leg.SetBorderSize(0)
leg.Draw("same")
lifetimecanv.SaveAs("lifetimecanv.pdf")


outfile.Write()
outfile.Close()

