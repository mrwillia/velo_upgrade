import os,sys
import ROOT
from math import *
from ROOT import *
import numpy as np
import LHCbStyle


f = TFile("outfile_27_07_2017_v3.root")
h = f.Get("hittree")

histtimecanv = TCanvas( 'histtimecanv', 'histtimecanv',200, 10, 700, 500 )

hitmapxy = TH2D("hitmapxy","hitmapxy", 100,-50,50, 100,-50,50)
hitmapzy = TH2D("hitmapzy","hitmapzy", 100,-300,800, 100,-50,50)

##hittree.Draw("ypos_hit:xpos_hit>>hitmapxy","module_num==10","COLZ")
##hittree.Draw("zpos_hit:ypos_hit>>hitmapzy","","COLZ")

for entry in h:
	
	hitmapzy.Fill(h.zpos_hit,h.ypos_hit)
	##if h.module_num==10 :
	##	hitmapxy.Fill(h.ypos_hit,h.xpos_hit)
	#xz_plot.Fill(h.zpos_hit,h.xpos_hit)
#	yz_plot.Fill(h.zpos_hit,h.xpos_hit)

##hitmapxy.GetXaxis().SetTitle("x(mm)")
##hitmapxy.GetYaxis().SetTitle("y(mm)")
##hitmapxy.SetStats(kFALSE)
##hitmapxy.Draw("COLZ")
##histtimecanv.SetRightMargin(0.15)
##histtimecanv.SaveAs("hitmapxy.png")

hitmapzy.GetXaxis().SetTitle("z(mm)")
hitmapzy.GetYaxis().SetTitle("y(mm)")
hitmapzy.Draw("COLZ")
histtimecanv.SetRightMargin(0.15)
histtimecanv.SaveAs("hitmapzy.png")

