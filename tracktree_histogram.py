import os,sys
import ROOT
from math import *
from ROOT import *
import numpy as np


f = TFile("outfile.root")
t = f.Get("tracktree")

histtimecanv = TCanvas( 'histtimecanv', 'histtimecanv',200, 10, 700, 500 )

eta1 = TH1D("eta1","nhits versus eta", 100,1.5,5.5)
eta1.Sumw2()

eta2 = TH1D("eta2","nhits versus eta", 100,1.5,5.5)
eta2.Sumw2()

eta1in = TH1D("eta1in","nhits_in versus eta", 100,1.5,5.5)
eta1in.Sumw2()

eta1out = TH1D("eta1out","nhits_out versus eta", 100,1.5,5.5)
eta1out.Sumw2()

phi1 = TH1D("phi1","nhits versus phi", 140,-3.5,3.5)
phi1.Sumw2()

phi2 = TH1D("phi2","nhits versus phi", 140,-3.5,3.5)
phi2.Sumw2()

phi1in = TH1D("phi1in","nhits_in versus phi", 140,-3.5,3.5)
phi1in.Sumw2()

phi1out = TH1D("phi1out","nhits_out versus phi", 140,-3.5,3.5)
phi1out.Sumw2()

etaphi1 = TH2D("etaphi1","nhits versus phi and eta", 50,1.5,5.5, 70,-3.5,3.5)
etaphi1.Sumw2()

etaphi2 = TH2D("etaphi2","nhits versus phi and eta", 50,1.5,5.5, 70,-3.5,3.5)
etaphi2.Sumw2()

etaphi1in = TH2D("etaphi1in","nhits_in versus phi and eta", 50,1.5,5.5, 70,-3.5,3.5)
etaphi1in.Sumw2()

etaphi1out = TH2D("etaphi1out","nhits_out versus phi and eta", 50,1.5,5.5, 70,-3.5,3.5)
etaphi1out.Sumw2()


for entry in t:
	
	eta1.Fill(t.eta,t.nhits)
	eta2.Fill(t.eta)
	eta1in.Fill(t.eta,t.nhits_in)
	eta1out.Fill(t.eta,t.nhits_out)
	phi1.Fill(t.phi,t.nhits)
	phi2.Fill(t.phi)
	phi1in.Fill(t.phi,t.nhits_in)
	phi1out.Fill(t.phi,t.nhits_out)
	etaphi1.Fill(t.eta,t.phi,t.nhits)
	etaphi2.Fill(t.eta,t.phi)
	etaphi1in.Fill(t.eta,t.phi,t.nhits_in)
	etaphi1out.Fill(t.eta,t.phi,t.nhits_out)
	

eta3 = eta1
eta3.Divide(eta2)
eta3.GetXaxis().SetTitle("#eta")
eta3.Draw()
histtimecanv.SaveAs("nhits_vs_eta_1D.root")

eta3in = eta1in
eta3in.Divide(eta2)
eta3in.GetXaxis().SetTitle("#eta")
eta3in.Draw()
histtimecanv.SaveAs("nhits_in_vs_eta_1D.root")

eta3out = eta1out
eta3out.Divide(eta2)
eta3out.GetXaxis().SetTitle("#eta")
eta3out.Draw()
histtimecanv.SaveAs("nhits_out_vs_eta_1D.root")

phi3 = phi1
phi3.Divide(phi2)
phi3.GetXaxis().SetTitle("#phi")
phi3.Draw()
histtimecanv.SaveAs("nhits_vs_phi_1D.root")

phi3in = phi1in
phi3in.Divide(phi2)
phi1in.GetXaxis().SetTitle("#phi")
phi3in.Draw()
histtimecanv.SaveAs("nhits_in_vs_phi_1D.root")

phi3out = phi1out
phi3out.Divide(phi2)
phi1out.GetXaxis().SetTitle("#phi")
phi3out.Draw()
histtimecanv.SaveAs("nhits_out_vs_phi_1D.root")

etaphi3 = etaphi1
etaphi3.Divide(etaphi2)
etaphi3.GetXaxis().SetTitle("#eta")
etaphi3.GetYaxis().SetTitle("#phi")
etaphi3.Draw("COLZ")
histtimecanv.SaveAs("nhits_vs_eta_vs_phi.root")

etaphi3in = etaphi1in
etaphi3in.Divide(etaphi2)
etaphi3in.GetXaxis().SetTitle("#eta")
etaphi3in.GetYaxis().SetTitle("#phi")
etaphi3in.Draw("COLZ")
histtimecanv.SaveAs("nhits_in_vs_eta_vs_phi.root")

etaphi3out = etaphi1out
etaphi3out.Divide(etaphi2)
etaphi3out.GetXaxis().SetTitle("#eta")
etaphi3out.GetYaxis().SetTitle("#phi")
etaphi3out.Draw("COLZ")
histtimecanv.SaveAs("nhits_out_vs_eta_vs_phi.root")


