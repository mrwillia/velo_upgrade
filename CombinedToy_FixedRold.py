import os,sys
import ROOT
from math import *
from ROOT import *

#Calculate radius of a track at a given z
def getR(eta,phi,z) :
   theta = 2*atan(exp(-eta))
   x = z*tan(theta)
   y = x*tan(phi)
   r = sqrt(x*x + y*y)
   #print z,x,y,r
   return r

#First sensor a track from a random PV passes through in z
def getfirstsensor(pvzpos) :
  firstsensor = {"A" : 0, "C" : 0}
  for side in ["A","C"] :
    for module in moduletimesep[side] :
      if 10.*pvzpos/3. > module :
        firstsensor[side] += 1
  return firstsensor

def getfirstsensorR(eta,phi,pvzpos) :
  theta = 2*atan(exp(-eta))
  firstsensor = {"A" : 0, "C" : 0}
  for side in ["A","C"] :
    for i,module in enumerate(modulespacesep[side]) :
      x = (module-pvzpos)*tan(theta)
      y = x*tan(phi)
      r = sqrt(x*x + y*y)
      if module > pvzpos and r > 5:
        firstsensor[side] += i
        break
  return firstsensor

#Lifetime calculation between secondary and primary vertex
def complifetime(bdecvertex,pv,bmom) :
  distance = sqrt(pow(bdecvertex[2]-pv[2],2)+\
                  pow(bdecvertex[1]-pv[1],2)+\
                  pow(bdecvertex[0]-pv[0],2) )                  
  return 5279.*distance/(300.*bmom.Mag())

#Calculate PV/SV time resolution
def calctimeres(timeres,pvflag,pvmult,pvtimepos,pvzpos) :
  if (pvflag):
     ntrackstosim = pvmult
  else:
     ntrackstosim = 2
  if ntrackstosim%2 != 0:
    ntrackstosim += 1
  # Get first sensor in z after PV
  #firstsensor = getfirstsensor(pvzpos)
  # Assume the clock has an edge at 0, and that module time offsets are relative to 0,0
  moduletimings = {"A" : [], "C" : []}
  timingforresidual = 0.
  ntracksinavg = 0
  for side in ["A","C"] :
    # One track in each side?
    for tracktosim in range(0,ntrackstosim/2) :
    #for tracktosim in range(0,ntrackstosim) :
      moduletimings[side].append([])
      # Half as many hits for pixels
      thistracklength = int(tracklen_histo.GetRandom())
      eta = eta_histo.GetRandom()
      phi = phi_histo.GetRandom()
      firstsensor = getfirstsensorR(eta,phi,pvzpos)
      #firstsensor = getfirstsensor(pvzpos)
      # If tracks passes through whole velo shorten to maximum number of hits
      if firstsensor[side]+thistracklength > len(moduletimesep[side]) - 1 :
        thistracklength = len(moduletimesep[side]) - firstsensor[side] - 1
      # Loop over hits and add a time of smeared by the resolution * (t_pv + time to sensor
      #assume last at r = 50
      lastsensor = thistracklength + firstsensor[side]
      for hit in range(0,thistracklength) :
        #if getR(eta,phi,modulespacesep[side][firstsensor[side]+hit]-pvzpos) > 45:
        moduletimings[side][-1].append(timeres*(int(pvtimepos + moduletimesep[side][firstsensor[side]+hit] - 10.*pvzpos/3.)/int(timeres)) + timeres/2. - \
                                                               (moduletimesep[side][firstsensor[side]+hit] - 10.*pvzpos/3.))
    #print sum(moduletimings["A"])/len(moduletimings["A"]),sum(moduletimings["C"])/len(moduletimings["C"]),pvtimepos
    for tracktiming in moduletimings[side] :
      # For each track take average of sensor timings
      if len(tracktiming) > 0:
        timingforresidual += sum(tracktiming)/len(tracktiming)
        ntracksinavg += 1
  # Average timing over all tracks
  if ntracksinavg > 0:
    timingforresidual /= ntracksinavg
    return timingforresidual-pvtimepos
  else:  
    return -1

mfile = TFile.Open("Brunel-VELOPP_nu76_VELOonly_10evt.root")
pvmult_histo = mfile.Get("TrackIPResolutionChecker/PV/RecoPVNumTracksH1");
tracklen_histo = mfile.Get("Track/PrChecker/Velo/velo_reconstructedHits")
eta_histo = mfile.Get("Track/PrChecker/Velo/velo_Eta_reconstructed")
phi_histo = mfile.Get("Track/PrChecker/Velo/velo_Phi_reconstructed")
#lumi              = 50 #e33 Use distributions here
lumi_histo = mfile.Get("Track/PrChecker/Velo/nPV_Events")
mfile2 = TFile.Open("VPTuple_cut.root")
tree = mfile2.Get("PrChecker/PrCheckerTuple")

resolution        = [10.,10.,75.] #mum
nonbpvresolution  = [20.,20.,150.] #mum
bresolution       = [20.,20.,300.] #mum
res_x_histo = mfile.Get("TrackIPResolutionChecker/PV/dxH1")
res_y_histo = mfile.Get("TrackIPResolutionChecker/PV/dyH1")
res_z_histo = mfile.Get("TrackIPResolutionChecker/PV/dzH1")

#bmomres           = [0.1,0.1,0.05] # %
#bmomcentre        = [1.0,1.0,0.85] # %
bmomres           = [0.005,0.005,0.005] # %
bmomcentre        = [1.0,1.0,1.0] # %

#beamspread        = [50.,50.,60000.] # mum
beamspread        = [37.7,37.7,63000.] # mum
#beamspread_time   = 300 #ps
beamspread_time   = 250 #ps

#ngen              = 100000
ngen              = 1000
sigmathreshold    = 3. 

timeres = 200.
modulespacesep = { "A" : [-277.,-252.,-227.,-202.,-132.,-62.,-37.,-12.,13.,38.,63.,88.,113.,138.,163.,188.,213.,238.,263.,325.,402.,497.,616.,661.,706.,751.], "C" : [-289.,-264.,-239.,-214.,-144.,-74.,-49.,-24.,1.,26.,51.,76.,101.,126.,151.,176.,201.,226.,251.,313.,390.,485.,604.,649.,694.,739.] }
moduletimesep = { "A" : [-277.,-252.,-227.,-202.,-132.,-62.,-37.,-12.,13.,38.,63.,88.,113.,138.,163.,188.,213.,238.,263.,325.,402.,497.,616.,661.,706.,751.] }
moduletimesep["C"] = []
# Calculate time separation based on z position
for i,module in enumerate(moduletimesep["A"]) :
  moduletimesep["A"][i] *= 10./3.
  moduletimesep["C"].append(moduletimesep["A"][i]-10.*12./3.)

bmomfile = TFile.Open("B_kinematics.root")
bmom_x_histo = bmomfile.Get("px")
bmom_y_histo = bmomfile.Get("py")
bmom_z_histo = bmomfile.Get("pz")

gRandom.SetSeed(0)
rnd = TRandom3()
rnd.SetSeed(0)

avgoverlap = 0.0
numbfliespast = 0
numbfliespast_andconfused = 0
avgiptobpv = 0

# For IP resolution
histo = TH1F("histo","histo",100,0,0.2) 
# For B vertex time resolution
histotime = TH1F("histotime","histotime",100,0,200) 

blifetimesall = TH1F("blifetimesall","blifetimesall",9,0,9) 
blifetimesconfused = TH1F("blifetimesconfused","blifetimesconfused",9,0,9) 
blifetimesall.Sumw2()
blifetimesconfused.Sumw2()

blifetimeresidual = TH1F("blifetimeresidual","blifetimeresidual",200,-3,3)
blifetimeresidualwithmismatch = TH1F("blifetimeresidualwithmismatch","blifetimeresidualwithmismatch",200,-3,3)

for event in range(0,ngen) :
  evtTimeFlag = True
  pvs = { "cart_pos" : [], 
          "time_pos" : [],
          "trk_mult" : [] }
  overlaps = 0 
  # Generate the PV positions in 4D
  #for pv in range(0,3*lumi) :
  lumi = int(lumi_histo.GetRandom())
  for pv in range(0,lumi) :
    pvs["cart_pos"].append([gRandom.Gaus(0,beamspread[0]),gRandom.Gaus(0,beamspread[1]),gRandom.Gaus(0,beamspread[2])])
    pvs["time_pos"].append(gRandom.Gaus(0,beamspread_time))
    pvs["trk_mult"].append(int(pvmult_histo.GetRandom()))    
  # Compute how often the PVs overlap spatially
  for pv1 in range(0,lumi) :
     for pv2 in range(pv1+1,lumi) :
       if abs(pvs["cart_pos"][pv1][2]-pvs["cart_pos"][pv2][2]) < sigmathreshold*resolution[2] :
        overlaps += 1
  avgoverlap+=float(overlaps)/(lumi)
  # pick a b vertex
  bvertex = int(lumi*gRandom.Rndm())
  # generate a b
  bmom = TVector3(bmom_x_histo.GetRandom(),bmom_y_histo.GetRandom(),bmom_z_histo.GetRandom())
  bmomunit = bmom.Unit()
  btau  = gRandom.Exp(450)/300.
  bctau = 300.*(bmom.Mag()/5279.)*btau
  blifetimesall.Fill(btau)
  # get b decay vertex,smeared
  bdecvertex = [pvs["cart_pos"][bvertex][0]+bmomunit[0]*bctau+gRandom.Gaus(0,bresolution[0]),\
                pvs["cart_pos"][bvertex][1]+bmomunit[1]*bctau+gRandom.Gaus(0,bresolution[1]),\
                pvs["cart_pos"][bvertex][2]+bmomunit[2]*bctau+gRandom.Gaus(0,bresolution[2])]
  # get the smeared PV position for the real b vertex
  bpvmeas    = [ pvs["cart_pos"][bvertex][0]+gRandom.Gaus(0,res_x_histo.GetRandom()*1000),
                 pvs["cart_pos"][bvertex][1]+gRandom.Gaus(0,res_y_histo.GetRandom()*1000),
                 pvs["cart_pos"][bvertex][2]+gRandom.Gaus(0,res_z_histo.GetRandom()*1000)]
  # Get the time resolution of the PV
  temp = calctimeres(timeres,True,pvs["trk_mult"][bvertex],pvs["time_pos"][bvertex],pvs["cart_pos"][bvertex][2]/1000.)
  if temp == -1:
    evtTimeFlag = False  
  bpvmeas_time = pvs["time_pos"][bvertex] + temp
  # get the smeared momentum
  bmomsmeared = bmom
  bmomsmeared.SetX(bmomsmeared.Px()*gRandom.Gaus(bmomcentre[0],bmomres[0]))
  bmomsmeared.SetY(bmomsmeared.Py()*gRandom.Gaus(bmomcentre[1],bmomres[1]))
  bmomsmeared.SetZ(bmomsmeared.Pz()*gRandom.Gaus(bmomcentre[2],bmomres[2]))
  bmomsmearedunit = bmomsmeared.Unit()
  # get the measured B tau
  btaumeas = complifetime(bdecvertex,bpvmeas,bmomsmeared)
  blifetimeresidual.Fill(btaumeas-btau)
  # the measured timestamp of the B vertex
  temp = calctimeres(timeres,False,pvs["trk_mult"][bvertex],pvs["time_pos"][bvertex],pvs["cart_pos"][bvertex][2]/1000.)
  if temp == -1:
    evtTimeFlag = False  
  btimepos = pvs["time_pos"][bvertex] + temp
  # now compute IP to PV
  decdotmom = ((bdecvertex[0]-bpvmeas[0])*bmomsmearedunit[0]+\
               (bdecvertex[1]-bpvmeas[1])*bmomsmearedunit[1]+\
               (bdecvertex[2]-bpvmeas[2])*bmomsmearedunit[2])
  IP = TVector3(bdecvertex[0] - bpvmeas[0] - decdotmom*bmomsmearedunit[0],\
                bdecvertex[1] - bpvmeas[1] - decdotmom*bmomsmearedunit[1],\
                bdecvertex[2] - bpvmeas[2] - decdotmom*bmomsmearedunit[2])
  iptobpv = IP.Mag()
  timetobpv = abs(btimepos-bpvmeas_time)
  histo.Fill(iptobpv/1000.)
  avgiptobpv += iptobpv
  # Now comes the loop to find the best associated PV of the SV
  gotconfused = False
  for pv1 in range(0,lumi) :
    if pv1 == bvertex : continue
    smearedpv = [ pvs["cart_pos"][pv1][0]+gRandom.Gaus(0,nonbpvresolution[0]),
                  pvs["cart_pos"][pv1][1]+gRandom.Gaus(0,nonbpvresolution[1]),
                  pvs["cart_pos"][pv1][2]+gRandom.Gaus(0,nonbpvresolution[2])] 
    if bdecvertex[2] > smearedpv[2] :      
      if smearedpv[2] > bpvmeas[2] :
        numbfliespast += 1
      decdotmom = ((bdecvertex[0]-smearedpv[0])*bmomsmearedunit[0]+\
                   (bdecvertex[1]-smearedpv[1])*bmomsmearedunit[1]+\
                   (bdecvertex[2]-smearedpv[2])*bmomsmearedunit[2])
      IP = TVector3(bdecvertex[0] - smearedpv[0] - decdotmom*bmomsmearedunit[0],\
                    bdecvertex[1] - smearedpv[1] - decdotmom*bmomsmearedunit[1],\
                    bdecvertex[2] - smearedpv[2] - decdotmom*bmomsmearedunit[2])
      temp = calctimeres(timeres,True,pvs["trk_mult"][pv1],pvs["time_pos"][pv1],pvs["cart_pos"][pv1][2]/1000.)
      if temp == -1:
        evtTimeFlag = False  
      thispvmeas_time = pvs["time_pos"][pv1] + temp
      if evtTimeFlag == True:
        # Check if this PV is closer to the SV in either space or time than 
        # the true parent, if so, we might get confused
        if (IP.Mag() < iptobpv) or (abs(btimepos-thispvmeas_time) < timetobpv) :
        #if (IP.Mag() < iptobpv) :
          # Now we decide which is the actually better PV
          if (1*iptobpv+10.*timetobpv > 1*IP.Mag()+10.*abs(btimepos-thispvmeas_time)) :
            gotconfused = True
            bpvmeas = smearedpv
            timetobpv = abs(btimepos-thispvmeas_time)
      else:
        if (IP.Mag() < iptobpv) :
          gotconfused = True
          bpvmeas = smearedpv
          timetobpv = -1
        
  if gotconfused :
    numbfliespast_andconfused += 1
  else : 
    blifetimesconfused.Fill(btau)
  # get the measured B tau
  btaumeas = complifetime(bdecvertex,bpvmeas,bmomsmeared)
  blifetimeresidualwithmismatch.Fill(btaumeas-btau)
  if timetobpv != -1:  
    histotime.Fill(timetobpv)  

print "Average IP to BPV =", float(avgiptobpv)/float(ngen)
print "Average PV overlap =", float(avgoverlap)/float(ngen)
print "Average PVs flown past =", float(numbfliespast)/float(ngen)
print "Average misID =", float(numbfliespast_andconfused)/float(ngen)
A = float(numbfliespast_andconfused)/float(ngen)
B = float(ngen)
print sqrt((A*A)/B)

histcanv = TCanvas("histcanv","histcanv")
histo.Draw("EP")
histcanv.SaveAs("histcanv.pdf")

gStyle.SetOptStat(0)
lifetimecanv = TCanvas("lifetimecanv","lifetimecanv")
#
blifetimesconfused.GetXaxis().SetTitle("B decay time [ps]")
blifetimesconfused.Divide(blifetimesall)
blifetimesconfused.Draw("P")
#
blifetimeresidual.GetXaxis().SetTitle("B #tau residual [ps]")
blifetimeresidual.GetXaxis().SetLabelSize(0.045)
blifetimeresidual.GetXaxis().SetTitleSize(0.045)
blifetimeresidual.GetYaxis().SetLabelSize(0.045)
blifetimeresidual.SetTitle("")
lifetimecanv.SetLogy()
blifetimeresidual.SetMarkerColor(2)
blifetimeresidual.SetMarkerStyle(4)
blifetimeresidual.SetLineColor(2)
blifetimeresidual.SetLineWidth(2)
blifetimeresidualwithmismatch.SetMarkerColor(4)
blifetimeresidualwithmismatch.SetMarkerStyle(4)
blifetimeresidualwithmismatch.SetLineColor(4)
blifetimeresidualwithmismatch.SetLineWidth(2)
blifetimeresidual.DrawNormalized("EP")
blifetimeresidualwithmismatch.DrawNormalized("EPSAME")
leg = TLegend(0.65, 0.75, 0.89, 0.89)
leg.AddEntry(blifetimeresidual, "Correct PV", "lp")
leg.AddEntry(blifetimeresidualwithmismatch, "Incorrect PV", "lp")
leg.SetTextSize(0.045)
leg.SetBorderSize(0)
leg.Draw("same")
lifetimecanv.SaveAs("lifetimecanv.pdf")

histtimecanv = TCanvas("histtimecanv","histtimecanv")
histotime.Draw("EP")
histtimecanv.SaveAs("histtimecanv.pdf")

